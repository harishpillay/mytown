This repository will contain all the software and documentation needed
to create a set of tools to help run municipal services.

The specific initial area of focus will be the Town Council management
software system for the Singapore Town Councils.

This is Free Software that is released on the GPLv3 license.

